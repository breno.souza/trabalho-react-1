import React, { Component } from 'react'
import Header from '../../components/Header';

import { listNotes, saveNotes, delNotes, revNotes,editNotes } from '../../services/serviceNotes'

import './style.css'

export default class Notes extends Component {
  
  state = {
    notes: [],
    loading: true,
    loadingSave: false,
    text: '',
    edit: false,
    exc : false,
    pes:  false
  }

  async componentDidMount () {
    this.setState({
      notes: await listNotes(),
      loading: false
    })
  }

  newNote = () => {
    this.setState({
      new: true
    })
  }

  editText = (event) => {
    this.setState({
      text: event.target.value
    })
  }

  saveNote = async () => {
    this.setState({
      loadingSave: true
    })
    await saveNotes(this.state.text)
    this.setState({
      notes: await listNotes(),
      notes: this.state.notes.reverse(),
      loadingSave: false,
      new: false,
      text: '',
      edit: false
    })
  }
  
  pesNote = () => {
    this.setState({
          pes: true
          
    })
    
  }   

  editNote = () => {
    this.setState({
           edit: true
          
    })
    
  }   

  editaNote = async () => {
    this.setState({
      notes: this.state.notes.reverse(),
      exc: true
    })
    await delNotes(this.state.text)
    this.setState({
      loadingSave: true
    })
    await saveNotes(this.state.text)
    this.setState({
      notes: await listNotes(),
      notes: this.state.notes.reverse(),
      loadingSave: false,
      new: false,
      text: '',
      edit: false,
      exc: false
    })
  }

  delNote = async () => {
    this.setState({
      notes: this.state.notes.reverse(),
      exc: true
    })
    await delNotes(this.state.text)
    this.setState({
      notes: await listNotes(),
      exc: false,
      new: false,
      text: ''
    })
  }

  render() {
    return (
      <div>
        <Header/>
        <div className='content'>
          
          <div className='filters'>
            <div className='left'>
              <input type='search' placeholder='Procurar...' />
              <button onClick={this.pesNote}>Pesquisar</button>
            </div>
            <div className='right'>
              <button onClick={this.newNote}>Nova nota</button>
            </div>
          </div>

          <hr />

          <div className='list'>
            { this.state.loading && (
              <div className='item item-empty'>
                Carregando anotações...
              </div>
            ) }
            { !this.state.notes.length && !this.state.loading && (
              <div className='item item-empty'>
                Nenhuma nota criada até o momento!
              </div>
            ) }
            { this.state.new && (
              <div className='item'>
                <input onChange={this.editText} className='left' type='text' placeholder='Digite sua anotação' />
                <div className='right'>
                  { !this.state.loadingSave && (<button onClick={this.saveNote}>Salvar</button>) }
                  { this.state.loadingSave && (<span>Salvando...</span>) }
                </div>
              </div>
            )}
             { this.state.exc && (
              <div className='item'>
                <div className='left'>
                  { (<span>Excluindo...</span>) }
                </div>
              </div>
            )}
            { this.state.edit && this.state.notes.map(note =>(
              <div className='item'>
                <input onChange={this.editText} className='left' type='text' defaultValue={note.text} placeholder='Digite sua anotação' />
                <div className='right'>
                  { !this.state.loadingSave && (<button onClick={this.editaNote}>Salvar</button>) }
                  { this.state.loadingSave && (<span>Salvando...</span>) }
                </div>
              </div>
            ))}
            { !this.state.edit && this.state.notes.map((note,index) => (
              <div className='item'>
                 {note.text}
                <div className='right'>
                  { !note.id && (<button onClick={this.saveNote}>Salvar</button>) }
                  { note.id && (<button onClick={this.delNote}>Excluir</button>) }
                  { note.id && (<button onClick={this.editNote}>Editar</button>) }
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}
